After you clone the repository you should see **7** different **CSS** projects:

Some of them are pretty simple like (**blogPage**), others are more complicated and interesting, like (**lakeTahoe**)

Implementation for all these projects was made with the help of pure **CSS** or with **Bootstrap3**

To run any of the above listed projects, just run the corresponding **.html** file from the root directory of the specific project.